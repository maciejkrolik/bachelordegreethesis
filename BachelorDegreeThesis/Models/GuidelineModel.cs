﻿using Microsoft.WindowsAzure.Storage.Table;

namespace BachelorDegreeThesis.Models
{
    public class GuidelineModel : TableEntity
    {
        public string Guideline { get; set; }
    }
}
