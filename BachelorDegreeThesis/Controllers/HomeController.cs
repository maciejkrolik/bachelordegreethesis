﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using BachelorDegreeThesis.Data;
using System;
using BachelorDegreeThesis.ViewModels;
using Newtonsoft.Json;

namespace BachelorDegreeThesis.Controllers
{
    public class HomeController : Controller
    {
        private readonly StorageHelper storageHelper = new StorageHelper();
        private readonly DBHelper dbHelper = new DBHelper();

        public async Task<IActionResult> Index()
        {
            var bachelorDegreeModel = await GetBachelorDegreeViewModelAsync();
            return View(bachelorDegreeModel);
        }

        [HttpPost]
        [ActionName("Index")]
        public async Task<IActionResult> IndexPost(string name, string lastName, string title, string img)
        {
            try
            {
                var queueItem = new
                {
                    Name = name,
                    LastName = lastName,
                    Title = title,
                    Img = img
                };
                await storageHelper.AddToQueue(JsonConvert.SerializeObject(queueItem));
                ViewBag.Message = $"Pomyślnie dodano do kolejki: {name} {lastName}, {title}, {img}";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Nie udało się dodać do kolejki. Błąd: " + ex.Message;
            }
            var bachelorDegreeModel = await GetBachelorDegreeViewModelAsync();
            return View(bachelorDegreeModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private async Task<BachelorDegreeViewModel> GetBachelorDegreeViewModelAsync()
        {
            var bachelorDegreeModel = new BachelorDegreeViewModel()
            {
                ThesisList = await dbHelper.GetThesisList(),
                GuidelineList = await storageHelper.GetGuidelineList()
            };

            bachelorDegreeModel.Base64Imgs = await storageHelper.GetBlobs(bachelorDegreeModel.ThesisList);

            return bachelorDegreeModel;
        }
    }
}
