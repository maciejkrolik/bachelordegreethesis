﻿using BachelorDegreeThesis.Models;
using System.Collections.Generic;

namespace BachelorDegreeThesis.ViewModels
{
    public class BachelorDegreeViewModel
    {
        public List<ThesisModel> ThesisList { get; set; }
        public List<GuidelineModel> GuidelineList { get; set; }
        public Dictionary<string, string> Base64Imgs { get; set; }
    }
}
