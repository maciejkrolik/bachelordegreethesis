﻿using BachelorDegreeThesis.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BachelorDegreeThesis.Data
{
    public class StorageHelper
    {
        private CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionStrings.STORAGE_CONNECTION_STRING);

        public async Task<List<GuidelineModel>> GetGuidelineList()
        {
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(ConnectionStrings.STORAGE_TABLE_NAME);
            TableContinuationToken token = null;

            var guidelines = new List<GuidelineModel>();
            do
            {
                var queryResult = await table.ExecuteQuerySegmentedAsync(new TableQuery<GuidelineModel>(), token);
                guidelines.AddRange(queryResult.Results);
                token = queryResult.ContinuationToken;
            } while (token != null);

            return guidelines;
        }

        public async Task<Dictionary<string, string>> GetBlobs(List<ThesisModel> thesisModels)
        {
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference(ConnectionStrings.STORAGE_CONTAINER_NAME);

            var imgs = new Dictionary<string, string>();
            foreach (var thesisModel in thesisModels)
            {
                try
                {
                    CloudBlob blob = blobContainer.GetBlockBlobReference(thesisModel.IDObrazka);

                    MemoryStream stream = new MemoryStream();
                    await blob.DownloadToStreamAsync(stream);
                    byte[] byteArray = stream.ToArray();

                    imgs.Add(thesisModel.IDObrazka, Convert.ToBase64String(byteArray));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    imgs.Add(thesisModel.IDObrazka, "");
                }
            }

            return imgs;
        }

        public async Task AddToQueue(string item)
        {
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference(ConnectionStrings.STORAGE_QUEUE_NAME);
            await queue.CreateIfNotExistsAsync();

            CloudQueueMessage message = new CloudQueueMessage(item);
            await queue.AddMessageAsync(message);
        }
    }
}
