﻿using BachelorDegreeThesis.Models;
using Microsoft.EntityFrameworkCore;

namespace BachelorDegreeThesis.Data
{
    public class DBContext : DbContext
    {
        public DbSet<ThesisModel> ThesisModels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionStrings.DB_CONNECTION_STRING);
        }
    }
}
