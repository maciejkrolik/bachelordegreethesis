﻿using BachelorDegreeThesis.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BachelorDegreeThesis.Data
{
    public class DBHelper
    {
        public async Task<List<ThesisModel>> GetThesisList()
        {
            var context = new DBContext();
            return await context.ThesisModels.ToListAsync();
        }
    }
}
