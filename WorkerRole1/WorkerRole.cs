using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json.Linq;

namespace WorkerRole1
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("WorkerRole1 is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            Trace.TraceInformation("WorkerRole1 has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("WorkerRole1 is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("WorkerRole1 has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");

                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionStrings.STORAGE_CONNECTION_STRING);

                // Queue
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                CloudQueue queue = queueClient.GetQueueReference(ConnectionStrings.STORAGE_QUEUE_NAME);

                // Blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(ConnectionStrings.STORAGE_CONTAINER_NAME);

                // DB
                DBContext dbContext = new DBContext();

                CloudQueueMessage peekedMessage = queue.GetMessage();
                if (peekedMessage != null)
                {
                    Trace.TraceInformation($"Peeked message: {peekedMessage.AsString}");

                    JObject queueItem = JObject.Parse(peekedMessage.AsString);

                    var idObrazka = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ") + ".png";

                    // Insert to DB
                    dbContext.ThesisModels.Add(new ThesisModel
                    {
                        IDObrazka = idObrazka,
                        Imie = queueItem["Name"].ToString(),
                        Nazwisko = queueItem["LastName"].ToString(),
                        Tytul = queueItem["Title"].ToString()
                    });
                    dbContext.SaveChanges();

                    // Upload image
                    CloudBlockBlob blob = blobContainer.GetBlockBlobReference(idObrazka);
                    blob.UploadFromStream(await GetImageAsStream(queueItem["Img"].ToString()));

                    queue.DeleteMessage(peekedMessage);
                }

                await Task.Delay(15000);
            }
        }

        public async Task<Stream> GetImageAsStream(string url)
        {
            using (var client = new HttpClient())
            {
                var byteArray = await client.GetByteArrayAsync(url);
                return new MemoryStream(byteArray);
            }
        }
    }
}
