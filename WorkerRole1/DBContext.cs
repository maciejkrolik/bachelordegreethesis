﻿using Microsoft.Azure;
using Microsoft.EntityFrameworkCore;

namespace WorkerRole1
{
    public class DBContext : DbContext
    {
        public DbSet<ThesisModel> ThesisModels { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(CloudConfigurationManager.GetSetting("DatabaseConnectionString"));
        }
    }
}
