﻿namespace WorkerRole1
{
    public class ThesisModel
    {
        public int ID { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Tytul { get; set; }
        public string IDObrazka { get; set; }
    }
}
