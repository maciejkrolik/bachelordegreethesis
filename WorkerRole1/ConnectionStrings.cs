﻿namespace WorkerRole1
{
    public static class ConnectionStrings
    {
        public static readonly string STORAGE_CONNECTION_STRING = "";
        public static readonly string STORAGE_TABLE_NAME = "";
        public static readonly string STORAGE_CONTAINER_NAME = "";
        public static readonly string STORAGE_QUEUE_NAME = "";
        public static readonly string DB_CONNECTION_STRING = "";
    }
}
